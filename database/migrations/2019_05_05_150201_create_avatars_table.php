<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvatarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avatars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->default('');
            $table->string('url')->default('');
            $table->timestamps();
        });

        DB::table('avatars')->insert(
            [ 
                ['name' => 'Squid',
                'url' => 'https://i.imgur.com/qpchZHB.jpg'],

                ['name' => 'PussyKiller',
                'url' => 'https://i.imgur.com/lQBxonG.png'],

                ['name' => 'Ripped',
                'url' => 'https://i.imgur.com/M7Oqb1O.png'],

                ['name' => 'Misha',
                'url' => 'https://i.imgur.com/3qQG62n.png'],

                ['name' => 'Retirded Sanic',
                'url' => 'https://i.imgur.com/uMdKPJb.png'],

                ['name' => 'extra large',
                'url' => 'https://i.imgur.com/HuH60ch.jpg'],

                ['name' => 'FunkyBebop',
                'url' => 'https://i.imgur.com/3kS7ulN.jpg'],

                ['name' => 'Damien du 64',
                'url' => 'https://i.imgur.com/FO2LvVM.png'],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avatars');
    }
}
