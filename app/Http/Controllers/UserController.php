<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Player;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    //Route::get('/createplayer')->name('User')
    public function index()
    {
        ////Return Create_Player form
        //check if user has no active player
        if (!Auth::user()->activePlayer()) {
            return view('create_player');
        } else {
            return redirect('/player');
        }
    }

    //Route::post('/createplayer')->name('UserCreatePlayer')
    public function create(Request $request)
    {
        ////Validation
        $validatedData = $request->validate([
            'name' => 'required|unique:players|between:5,25',
            'picture' => 'required|integer|between:1,8',
        ]);
        ////Create Player
        //Insert into 'players' table
        $player = new Player;
        $player->user_id = Auth::user()->id;
        $player->avatar_id = $request->picture;
        $player->name = $request->name;
        $player->is_active = 1;
        $player->save();

        return redirect('/player');
    }
}
