<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Player;
use App\Gang;

class GangController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('hasplayer');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        if (Auth::user()->activePlayer()->activeGang()) {
            return view('gang/index');
        }
        return view('gang/create_gang');
    }

    //Créer Gang
    public function create(Request $request)
    {
        ////Validation
        $validatedData = $request->validate([
            'name' => 'required|unique:gangs|between:3,30',
            'color' => 'required',
        ]);
        ////Create Gang
        //Insert into 'invits' table
        $gang = new Gang;
        $gang->name = $request->name;
        $gang->color = $request->color;
        $gang->money = 0;
        $gang->gang_status = 1;
        $gang->save();

        //Update Player
        $player = Auth::user()->activePlayer();
        $player->gang_id = $gang->id;
        $player->rank_id = 3;
        $player->save();

        return redirect('/gang');
    }

    public function sends_invite(Request $request)
    {
        $validatedData = $request->validate([
            'player' => 'required',
        ]);

        //Create Invite
        $player = Player::whereName($request->player)->first();
        $gang = Auth::user()->activePlayer()->activeGang();
        $player->gangs()->attach($gang);

        return redirect('/gang');
        
    }

    public function accepts_invite(Request $request)
    {
        $validatedData = $request->validate([
            'gang' => 'required',
        ]);

        //Update Player
        $player = Auth::user()->activePlayer();
        $gang = Gang::whereName($request->gang)->first();
        $player->gang_id = $gang->id;
        $player->rank_id = 2;
        $player->save();

        //Delete Invite
        $player->gangs()->detach($gang);

        return redirect('/gang');
    }

    public function cancel_invite(Request $request)
    {
        $validatedData = $request->validate([
            'player' => 'required',
            'gang' => 'required',
        ]);
        
        $player = Player::whereName($request->player)->first();
        $gang = Gang::whereName($request->gang)->first();
        //Delete Invite
        $player->gangs()->detach($gang);
        return redirect('/gang');
    }

    public function leaves_gang(Request $request)
    {
        $validatedData = $request->validate([
            'player' => 'required',
            'gang' => 'required',
        ]);

        $player = Player::whereName($request->player)->first();
        $gang = Gang::whereName($request->gang)->first();
        
        //Lieutenant deletes Gang
        if ($player->rank_id > 2) {
            foreach ($gang->soldats as $soldat) {
                $soldat->gang_id = 0;
                $soldat->rank_id = 1;
                $soldat -> save();
            };
            foreach ($gang->invits as $member) {
                $member->gangs()->detach($gang);
            };
            $gang->delete();
        }
        //Player leaves Gang
        $player->gang_id = 0;
        $player->rank_id = 1;
        $player-> save();
        
        return redirect('/gang');
    }
}
