<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class HasPlayer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if(!$user->activePlayer()){
            return redirect('/createplayer');
        }
        
        return $next($request);

    }
}
