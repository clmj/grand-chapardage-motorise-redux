<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsLieutenant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $player = Auth::user()->activePlayer();

        if($player->rank_id < 3){
            return redirect('/gang');
        }
        
        return $next($request);

    }
}
