<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turf extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gang_id', 'name', 'value',
    ];
    
    public function gang(){
        return $this->belongsTo('App\Gang');
    }
}
