<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gang extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'color', 'money', 'gang_status',
    ];

    public function players(){
        return $this->hasMany('App\Player');
    }

    public function turfs(){
        return $this->hasMany('App\Turf');
    }

    public function clan(){
        return $this->belongsTo('App\Clan');
    }

    public function lieutenant(){
        return $this->players()->where('rank_id', '>', '2')->first();
    }

    public function soldats() {
        return $this->players()->where('rank_id', '=', '2');
    }
    //invitations
    public function invits(){
        return $this->belongsToMany('App\Player');
    }
}
