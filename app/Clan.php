<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'leadGang_id', 'name', 'money',
    ];

    public function gangs(){
        return $this->hasMany('App\Gang');
    }

    public function leadgang(){
        return $this->leadGang_id;
    }
}