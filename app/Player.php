<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'avatar_id', 'gang_id', 'rank_id', 'name', 'money', 'is_active',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function avatar(){
        return $this->belongsTo('App\Avatar');
    }

    public function rank(){
        return $this->belongsTo('App\Rank');
    }

    public function gang(){
        return $this->belongsTo('App\Gang');
    }

    public function activeGang(){
        return $this->gang()->where('gang_status', '=', '1')->first();
    }
    //invitations
    public function gangs(){
        return $this->belongsToMany('App\Gang');
    }
}
