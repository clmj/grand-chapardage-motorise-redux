<?php
use App\Player;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//SplashScreen
Route::get('/', function () {
    if(Auth::check()){
        return redirect('/player');
    };
    return view('homepage');
});

Auth::routes();

//User
/////Formulaire de Création du Personnage
Route::get('/createplayer', 'UserController@index')->name('User');
/////Insert into 'players'
Route::post('/createplayer', 'UserController@create')->name('UserCreatePlayer');

//Player
Route::get('/player', 'PlayerController@index')->name('Player');

//Gang
Route::get('/gang', 'GangController@index')->name('Gang');
////Insert into 'gangs'
Route::post('/gang', 'GangController@create')->name('PlayerCreatesGang');
////Insert into pivot 'gang_player'
Route::post('/send_invite', 'GangController@sends_invite')->name('LieutenantInvitesPlayer')->middleware('islieutenant');
//Update 'player' & Delete from pivot 'gang_player'
Route::post('/accept_invite', 'GangController@accepts_invite')->name('PlayerAcceptsInvite');
//Delete from pivot 'gang_player'
Route::post('/delete_invite', 'GangController@cancel_invite')->name('InvitationCancelled');
//Update 'player' || Update 'player' & Delete from 'gangs' & from pivot 'gang_player'
Route::post('/leaves_gang', 'GangController@leaves_gang')->name('PlayerLeavesGang');
//
Route::get('/test', function(){
    //return Auth::user()->activePlayer()->activeGang()->id;
    //$player = Player::whereName('Misha4kids');
    //return $player->first()->name;
    //dd(Auth::user()->activePlayer()->activeGang()->invits());
    //return Auth::user()->activePlayer()->gangs;
    //return Auth::user()->activePlayer()->rank->id;
    //return Auth::user()->activePlayer()->activeGang()->invits;
    return Auth::user()->activePlayer()->gangs->first();
});