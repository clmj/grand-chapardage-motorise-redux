@php
    use App\Gang;
    use App\Rank;
@endphp

@extends('layouts.app')

@section('content')
<div class="container-fluid mt-3 mb-3">
    <div class="row justify-content-center">

            
        <div class="col-sm-4">
            <div class="card text-center">
                <div class="card-header">{{ Auth::User()->activePlayer()->name }}</div>
                <div class="card-body d-flex flex-column align-items-center">
                    <img style='width:12em;' src={{ Auth::User()->activePlayer()->avatar->url }}>
                    {{-- Rang --}}
                    <p class="mx-auto p-3">Rang : {{ Rank::find(Auth::User()->activePlayer()->rank_id)->name }}<p>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
