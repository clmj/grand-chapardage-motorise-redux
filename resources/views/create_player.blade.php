@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ ('Création du personnage') }}</div>

                <div class="card-body justify-content-center">
                    <form method="POST" action="{{ route('UserCreatePlayer') }}">
                        @csrf
                        <div class="form-group row">
                            {{-- Name --}}
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        {{-- Picture --}}
                        <div>
                            <fieldset class="form-group row mx-auto">
                                <legend class="mx-auto col-form-label">Picture</legend>
                                {{-- Récuperer les Avatars depuis la base de données --}}
                                @php
                                    $avatars = App\Avatar::all();
                                @endphp
                                
                                @foreach ($avatars as $avatar)
                                    <input type="radio" name="picture" id="{{$avatar->id}}"  value="{{$avatar->id}}" class="sr-only form-control @error('name') is-invalid @enderror" required>
                                    <label for="{{$avatar->id}}">
                                        <img style="height: 8em; width: 10em;" src="{{$avatar->url}}" alt="{{$avatar->name}}">
                                    </label>
                                @endforeach
                                @error('picture')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                        </div>

                        <button type="submit" class="btn btn-primary">
                            {{ ('Valider') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
