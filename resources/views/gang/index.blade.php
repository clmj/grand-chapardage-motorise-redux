@php
use App\Player;
@endphp

@extends('layouts.app')

@section('content')
<div class="container-fluid mt-3 mb-3">
    <div class="row justify-content-center">

            <div class="col-sm-6 mb-3">

                <div class="card text-center">
                    <div class="card-header">Fiche de Gang</div>

                    <div class="card-body">

                        {{-- Fiche Gang --}}
                        <p style="color: {{Auth::user()->activePlayer()->activeGang()->color}}; font-size: 2em;">{{ Auth::user()->activePlayer()->activeGang()->name }}</p>
                        <p>{{ Auth::user()->activePlayer()->activeGang()->money }} $ dans la banque</p>
                        <div class="card-header">Lieutenant</div>
                        <p>
                            <img style="height: 6em; width: 6em;" src='{{ Auth::user()->activePlayer()->activeGang()->lieutenant()->avatar->url }}'>
                            {{ Auth::user()->activePlayer()->activeGang()->lieutenant()->name }}
                        </p>

                        {{-- Liste Membres --}}
                        @if (Auth::user()->activePlayer()->activeGang()->soldats()->exists())
                            <div class="card-header">Membres</div>
                            <ul class="list-group list-group-flush">
                                @foreach (Auth::user()->activePlayer()->activeGang()->soldats as $soldat)
                                    <li class="list-group-item col-md-8 mx-auto d-flex align-items-center justify-content-around">
                                        <img style="height: 4em; width: 4em;" src='{{ $soldat->avatar->url }}'>
                                        {{$soldat->name}}
                                        {{--Virer--}}
                                        @if (Auth::user()->activePlayer()->rank_id > 2)
                                            <form method="POST" action="{{ route('PlayerLeavesGang') }}">
                                                <input name="player" type="hidden" value="{{ $soldat->name }}">
                                                <input name="gang" type="hidden" value="{{ $soldat->activeGang()->name }}">
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger">
                                                        {{ ('Virer') }}
                                                </button>
                                            </form>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        @endif

                        {{--Bouton supprimer/quitter Gang--}} 
                        <form method="POST" action="{{ route('PlayerLeavesGang') }}">
                            <input name="player" type="hidden" value="{{ Auth::user()->activePlayer()->name }}">
                            <input name="gang" type="hidden" value="{{ Auth::user()->activePlayer()->activeGang()->name }}">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-primary">
                                @if (Auth::user()->activePlayer()->rank_id > 2)
                                    {{ ('Supprimer le Gang') }}
                                @else
                                    {{ ('Quitter le gang') }}
                                @endif
                            </button>
                        </form>
                    </div>

                </div>

            </div>

            @php
                $invités = false;
                $invitation = false;
            @endphp

            @if (Auth::user()->activePlayer()->rank_id > 2)

                @if (Auth::user()->activePlayer()->activeGang()->invits()->exists())
                    @php
                        $invités = true;
                    @endphp
                @endif

                @php
                    $playersList = [];
                @endphp
                @foreach (Player::all() as $player)
                    @if (
                        $player->id == Auth::user()->activePlayer()->id 
                        || $player->gang_id == Auth::user()->activePlayer()->gang_id
                        || Auth::user()->activePlayer()->activeGang()->invits->contains($player)
                        )
                    @else
                        @php
                            array_push($playersList, $player);
                        @endphp
                    @endif
                @endforeach

                @if (!empty($playersList))
                    @php
                        $invitation = true;
                    @endphp
                @endif
            @endif

            @if ($invités || $invitation)

                <div class="col-sm-4">

                    @if (Auth::user()->activePlayer()->activeGang()->invits()->exists())
                        <div class="card">
                            <div class="card-header text-center">Invités</div>
                            <div class="card-body">
                                <ul class="list-group list-group-flush">
                                    @foreach (Auth::user()->activePlayer()->activeGang()->invits as $invite)
                                        <li class="list-group-item col-md-12 mx-auto d-flex align-items-center justify-content-around">
                                            <img style="height: 4em; width: 4em;" src='{{ $invite->avatar->url }}'>
                                            {{$invite->name}}
                                            <form method="POST" action="{{ route('InvitationCancelled') }}">
                                                <input name="player" type="hidden" value="{{ $invite->name }}">
                                                <input name="gang" type="hidden" value="{{ Auth::user()->activePlayer()->activeGang() }}">
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger mx-auto">
                                                    Annuler
                                                </button>
                                            </form>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif

                    @if (!empty($playersList))
                        <div class="card mt-3">
                            <div class="card-header text-center">Inviter</div>
                            <div class="card-body">
                                <form method="POST" action="{{ route('LieutenantInvitesPlayer') }}">
                                    @csrf
                                    <div class="form-group row">
                                        <div class="col-md-8">
                                            <select id="player" class="form-control @error('player') is-invalid @enderror" name="player" value="{{ old('player') }}" required autocomplete="player" autofocus>
                                                @foreach ($playersList as $player)
                                                    <option>{{$player->name}}
                                                @endforeach
                                            </select>
                                            @error('player')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <button type="submit" class="btn btn-primary mx-auto">
                                            Inviter
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    @endif

                </div>

            @endif

                
    </div>
</div>
@endsection
