@extends('layouts.app')

@section('content')
<div class="container-fluid mt-3 mb-3">
    <div class="row justify-content-center">
        
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header text-center">Créez votre gang</div>
                <div class="card-body" style="text-align: center;">
                    <form method="POST" action="{{ route('PlayerCreatesGang') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-3 col-form-label text-md-right">{{ ('Nom') }}</label>

                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>
                        
                        <div class="form-group row">
                            <label for="Couleur" class="col-md-3 col-form-label text-md-right">{{ ('Couleur') }}</label>

                            <div class="col-md-8">
                                <select id="color" class="form-control @error('picture') is-invalid @enderror" name="color" value="{{ old('color') }}" required autocomplete="color" autofocus>
                                    <option style="color:#e50d0d;">#e50d0d
                                    <option style="color:#235789;">#235789
                                    <option style="color:#f1d302;">#f1d302
                                    <option style="color:#f77f00;">#f77f00
                                    <option style="color:#ec0868;">#ec0868
                                </select>

                                @error('picture')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>
                        
                        <div class="row">
                            <div class="mx-auto">
                                <button type="submit" class="btn btn-primary">
                                    {{ ('Valider') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @if (Auth::user()->activePlayer()->gangs()->exists())
                <div class="card">
                    <div class="card-header text-center">Invitations</div>
                    <div class="card-body" style="text-align: center;">
                        <ul class="list-group list-group-flush">
                            @foreach (Auth::user()->activePlayer()->gangs as $invite)
                                <li class="list-group-item d-flex align-items-center justify-content-around">
                                        {{ $invite->name }}
                                    <div class="row ml-1">
                                    <form method="POST" action="{{ route('PlayerAcceptsInvite') }}">
                                        <input name="gang" type="hidden" value="{{ $invite->name }}">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-success mr-1 mb-1">
                                            Accepter
                                        </button>
                                    </form>
                                    <form method="POST" action="{{ route('InvitationCancelled') }}">
                                        <input name="player" type="hidden" value="{{ Auth::user()->activePlayer()->name }}">
                                        <input name="gang" type="hidden" value="{{ $invite->name }}">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-danger">
                                            Décliner
                                        </button>
                                    </form>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection
