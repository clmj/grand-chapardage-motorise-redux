<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Courgette|Roboto" rel="stylesheet"> 

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-light" style="border-bottom: 5px solid black;">
            <div class="container">
                @guest
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'GCM') }}
                    </a>
                @else
                    <a class="navbar-brand" href="{{ url('/player') }}">
                        {{ config('app.name', 'GCM') }}
                    </a>
                @endguest
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="dropdown-item" href="{{ route('login') }}">Se connecter</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="dropdown-item" href="{{ route('register') }}">S'inscrire</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item">
                            {{-- Vers page Player --}}
                            @if (Auth::user()->activePlayer())
                                <li class="nav-item">
                                    <a class="dropdown-item" href="{{ route('Player') }}">
                                        {{ Auth::user()->activePlayer()->name }}
                                    </a>
                                </li>
                                {{-- Vers page Gang --}}
                                @if (Auth::user()->activePlayer()->activeGang())
                                    <li class="nav-item">
                                        <a class="dropdown-item" href="{{ route('Gang') }}">
                                            {{ Auth::user()->activePlayer()->activeGang()->name }}
                                        </a>
                                    </li>
                                @else
                                    <li class="nav-item">
                                        <a class="dropdown-item" href="{{ route('Gang') }}">
                                            Gang
                                            {{-- //Notification Invitation --}}
                                            @if(Auth::user()->activePlayer()->gangs->isNotEmpty())
                                                <span class="badge badge-primary">{{ count(Auth::user()->activePlayer()->gangs) }}</span>
                                            @endif
                                        </a>
                                    </li>
                                @endif
                            @else 
                                <li class="nav-item">
                                    <a class="dropdown-item" href="{{ route('Player') }}">
                                        Personnage
                                    </a>
                                </li>
                            @endif
                                    
                            {{-- Deconnexion  --}}
                            <li class="nav-item">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Se déconnecter
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main>
            @yield('content')
        </main>

        <footer class="navbar navbar-light bg-light justify-content-center" style="border-top: 5px solid black;">
            <span class="navbar-text">
                Clmj - 2019
            </span>
        </footer>
    </div>
</body>
</html>
